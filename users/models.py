from django.db import models
from django.contrib.auth.models import AbstractUser #easier to use to create custom user base classes

class CustomUser(AbstractUser):
    #first field is database related, can store database value as NULL meaning nothing
    #second field is validation related, form will allow an empty value
    #commonly used together in order to save blank values
    age = models.PositiveIntegerField(null = True, blank = True)
