from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse


def increment_po_number():

    try:
        last_po_number = Article.objects.latest('id')
    except Article.DoesNotExist:
        return 'PO' + str(1000)

    new_po_num = int(last_po_number.po_number[2:])
    new_po_num+=1
    return 'PO' + str(new_po_num)

class Article(models.Model):

    po_number = models.CharField(max_length=255, default=increment_po_number, editable=False)

    supplier_name = models.CharField(max_length=255)
    supplier_address = models.CharField(max_length=255)
    supplier_parts = models.TextField()

    date =  models.DateTimeField(auto_now_add=True)

    author = models.ForeignKey(
        get_user_model(),
        on_delete = models.CASCADE,
    )



    #string that gets displayed
    def __str__(self):
        return self.po_number

    def get_absolute_url(self):
        return reverse('article_detail', args=[str(self.id)])